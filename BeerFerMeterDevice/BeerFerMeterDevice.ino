#include <ESP8266WiFi.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS D6
#define WIFI_CONNECT_TRIES_LIMIT 10

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature DS18B20(&oneWire);
WiFiClient client;

float temperature;
const char* ssid ="";
const char* password = "";
long signalStrength;
int batteryVoltage;
const char* deviceId = "1629f32a-369c-48e2-bf87-63f0f9e9bb52";
const char* temperatureSensorId = "84639758-2120-4fa4-8f03-133246844c76";

const char* apiHost = "192.168.1.2";
const int apiPort = 8080;
const String apiUrl = String("/api/device/") + deviceId + "/measurements";

void setup() {
  Serial.begin(115200);
  while(!Serial) { }
  Serial.setTimeout(2000);
  Serial.println();
  Serial.println();
  Serial.println("Waked up.");
    
  DS18B20.begin();
  getTemperature();
  printTemperature();

  setupWiFiConnection();

  sendResult();

  Serial.println(String("Going sleep for ") + 600 + "s...");
  ESP.deepSleep(600e6);
}

float getTemperature() {
  do {
    DS18B20.requestTemperatures(); 
    temperature = DS18B20.getTempCByIndex(0);
    delay(100);
  } while (temperature == 85.0 || temperature == (-127.0));
}

void printTemperature() {
  char temperatureString[6];
  dtostrf(temperature, 2, 2, temperatureString);
  Serial.print("Temperature is: ");
  Serial.println(temperatureString);
}

void setupWiFiConnection() {  
  Serial.print(String("Connecting to network: '") + ssid + "' ");
  WiFi.begin(ssid, password);

  int tries = 0;
  while (WiFi.status() != WL_CONNECTED) {
    if (tries >= WIFI_CONNECT_TRIES_LIMIT) {
      Serial.println("WiFi connection setup failed.");
      return;
    }
    delay(500);
    Serial.print(".");
    tries++;
  }
  Serial.println("\r\nWiFi connected.");
  
  signalStrength = WiFi.RSSI();
  Serial.println(String("Signal strength: ") + signalStrength + " dbm\r\n");
}

void sendResult() {
  Serial.println(String("Connecting to host: '") + apiHost + ":" + apiPort + "' ...");
  
  if (!client.connect(apiHost, apiPort)) {
    Serial.println("Connection failed.");
    return;
  }

  Serial.print("Requesting URL: ");
  Serial.println(apiUrl);

  String data = assembleSingleMeasurementData();
  Serial.println(data);
  
  // This will send the request to the server
  client.print(String("POST ") + apiUrl + " HTTP/1.1\r\n" +
               "Host: " + apiHost + "\r\n" + 
               "Content-Type: application/json\r\n" + 
               "Content-Length: " + String(data.length()) + "\r\n" +
               "Connection: close\r\n\r\n");
  client.print(data);
  Serial.println();
  
  handleResponse();
  
  Serial.println("Closing connection...");
}

String assembleSingleMeasurementData() {
  char temperatureString[6];
  dtostrf(temperature, 2, 2, temperatureString);
  
  String data = String("{ ")
    + "\"sensorId\": \"" + temperatureSensorId + ", "
    + "\"measurement\": { \"value\": " + temperatureString + ", \"time"\: " + 0 + " }"
    + " }";
  return data;
}

String assembleHealthReportData() {
  String data = String("{ ")
    + "\"signalStrength\": " + String(signalStrength) + ", "
    + "\"batteryVoltage\": " + String(batteryVoltage)
    + " }";
  return data;
}

void handleResponse() {
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println("ERROR: Client timeout!");
      client.stop();
      return;
    }
  }
  Serial.println("Response:");
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
}

void loop() {
}

