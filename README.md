# BeerFerMeter

Application provides solution for fermentation process parameters inspection. Dedicated device supports temperature and gravity measuring. 

## Built With

* [Spring Boot](https://spring.io/projects/spring-boot) - The framework used
* [Gradle](https://gradle.org/) - Build tool
* [ESP8266 Arduino Core](https://arduino-esp8266.readthedocs.io/en/latest/) - Set of libraries used by device driver
