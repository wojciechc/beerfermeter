package pl.manufakturaczarnoziem.beerfermeter.sensor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface SensorTypeRepository extends JpaRepository<SensorType, Long> {
}
