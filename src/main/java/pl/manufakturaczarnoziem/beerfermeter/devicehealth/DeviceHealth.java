package pl.manufakturaczarnoziem.beerfermeter.devicehealth;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.manufakturaczarnoziem.beerfermeter.device.Device;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "device_health_report")
public class DeviceHealth {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long deviceHealthReportId;

    private BigDecimal signalStrength;

    private BigDecimal batteryVoltage;

    @OneToOne(mappedBy = "healthReport")
    private Device device;

}
