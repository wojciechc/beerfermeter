package pl.manufakturaczarnoziem.beerfermeter.devicehealth;

class HealthDataNotFoundException extends RuntimeException {

    public HealthDataNotFoundException() {
        super();
    }

    public HealthDataNotFoundException(String message) {
        super(message);
    }

    public HealthDataNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
