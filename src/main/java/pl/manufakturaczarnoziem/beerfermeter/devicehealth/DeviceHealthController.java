package pl.manufakturaczarnoziem.beerfermeter.devicehealth;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.manufakturaczarnoziem.beerfermeter.api.DeviceHealthApiData;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/devices")
public class DeviceHealthController {

    private final DeviceHealthService deviceHealthService;


    @PostMapping(value = "/{deviceId}/health", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity saveHealthData(
            @PathVariable Long deviceId,
            @RequestBody DeviceHealthApiData healthApiData) {
        log.debug("saveHealthData() deviceId: '{}', deviceHealthApiData: '{}'", deviceId, healthApiData);

        deviceHealthService.saveHealthData(deviceId, healthApiData);
        return ResponseEntity.ok().build();
    }
}
