package pl.manufakturaczarnoziem.beerfermeter.devicehealth;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.manufakturaczarnoziem.beerfermeter.device.Device;

@Repository
interface DeviceHealthRepository extends JpaRepository<DeviceHealth, Long> {
    Optional<DeviceHealth> findByDevice(Device device);
}
