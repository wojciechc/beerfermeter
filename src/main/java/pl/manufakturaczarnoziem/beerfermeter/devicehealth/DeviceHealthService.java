package pl.manufakturaczarnoziem.beerfermeter.devicehealth;

import java.util.Optional;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.manufakturaczarnoziem.beerfermeter.api.DeviceHealthApiData;
import pl.manufakturaczarnoziem.beerfermeter.device.Device;
import pl.manufakturaczarnoziem.beerfermeter.device.DeviceNotFoundException;
import pl.manufakturaczarnoziem.beerfermeter.device.DeviceRepository;

@Slf4j
@RequiredArgsConstructor
@Service
class DeviceHealthService {

    private final DeviceHealthRepository deviceHealthRepository;
    private final DeviceRepository deviceRepository;


    void saveHealthData(Long deviceId, DeviceHealthApiData healthApiData) {
        Optional<Device> maybeDevice = deviceRepository.findById(deviceId);
        maybeDevice
                .map(device -> {
                    Optional<DeviceHealth> maybeHealthReport = deviceHealthRepository.findByDevice(device);

                    DeviceHealth healthData = maybeHealthReport.orElseGet(() -> {
                        DeviceHealth newHealthReport = new DeviceHealth();
                        newHealthReport.setDevice(device);
                        return newHealthReport;
                    });
                    healthData.setSignalStrength(healthApiData.getSignalStrength());
                    healthData.setBatteryVoltage(healthApiData.getBatteryVoltage());

                    return deviceHealthRepository.save(healthData);
                })
                .orElseThrow(() -> new DeviceNotFoundException("Device '" + deviceId + "' not found!"));
    }

}
