package pl.manufakturaczarnoziem.beerfermeter.process;

enum ProcessState {
    CREATED,
    ACTIVE,
    TERMINATED
}
