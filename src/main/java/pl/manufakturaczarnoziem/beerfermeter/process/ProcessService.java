package pl.manufakturaczarnoziem.beerfermeter.process;

import java.util.List;

import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableList;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.manufakturaczarnoziem.beerfermeter.device.Device;
import pl.manufakturaczarnoziem.beerfermeter.device.DeviceRepository;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProcessService {

    private final ProcessRepository processRepository;
    private final DeviceRepository deviceRepository;


    public boolean isAnyProcessActiveForDevice(Long deviceId) {
        Device device = deviceRepository.getOne(deviceId);

        List<Process> processes = processRepository.findByDevicesInAndState(ImmutableList.of(device), ProcessState.ACTIVE);

        return !processes.isEmpty();
    }
}
