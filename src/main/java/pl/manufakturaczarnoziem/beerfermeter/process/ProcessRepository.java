package pl.manufakturaczarnoziem.beerfermeter.process;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.manufakturaczarnoziem.beerfermeter.device.Device;

@Repository
interface ProcessRepository extends JpaRepository<Process, Long> {
    List<Process> findByDevicesInAndState(List<Device> device, ProcessState state);
}
