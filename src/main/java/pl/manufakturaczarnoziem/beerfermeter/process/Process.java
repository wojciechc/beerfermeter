package pl.manufakturaczarnoziem.beerfermeter.process;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.manufakturaczarnoziem.beerfermeter.device.Device;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "process")
public class Process {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long processId;

    @Enumerated(value = EnumType.STRING)
    private ProcessState state;

    @ManyToMany
    @JoinTable(
            name = "process_device",
            joinColumns = @JoinColumn(name = "process_id"),
            inverseJoinColumns = @JoinColumn(name = "device_id"))
    private Set<Device> devices;
}
