package pl.manufakturaczarnoziem.beerfermeter.measurement;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.manufakturaczarnoziem.beerfermeter.sensor.Sensor;

@Repository
interface MeasurementRepository extends JpaRepository<Measurements, Long> {
    List<Measurements> findBySensor(Sensor sensor);
}
