package pl.manufakturaczarnoziem.beerfermeter.measurement;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.manufakturaczarnoziem.beerfermeter.api.MeasurementApiData;
import pl.manufakturaczarnoziem.beerfermeter.api.MeasurementsBundleApiData;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/devices")
public class MeasurementDataController {

    private final MeasurementDataService measurementDataService;


    @PostMapping(value = "/{deviceId}/measurements", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity saveMeasurementData(
            @PathVariable Long deviceId,
            @RequestBody MeasurementApiData measurementApiData) {
        log.debug("saveMeasurementData() deviceId: '{}', measurementApiData: '{}'", deviceId, measurementApiData);

        measurementDataService.saveMeasurement(deviceId, measurementApiData);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/{deviceId}/measurements-bundle", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity saveMeasurementsBundleData(
            @PathVariable Long deviceId,
            @RequestBody MeasurementsBundleApiData measurementsBundleApiData) {
        log.debug("saveMeasurementsBundleData() deviceId: '{}', measurementsBundleApiData: '{}'", deviceId, measurementsBundleApiData);

        measurementDataService.saveMeasurementsBundle(deviceId, measurementsBundleApiData);
        return ResponseEntity.ok().build();
    }
}
