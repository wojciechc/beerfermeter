package pl.manufakturaczarnoziem.beerfermeter.measurement;

import java.util.LinkedHashSet;

import lombok.Value;

@Value
class MeasurementsBundle {
    private LinkedHashSet<Measurement> measurements;
}
