package pl.manufakturaczarnoziem.beerfermeter.measurement;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import pl.manufakturaczarnoziem.beerfermeter.measurement.exception.MeasurementsDataIntegrityViolationException;

@Slf4j
@RequiredArgsConstructor
@Component
class MeasurementsBundleSerializer {

    private final ObjectMapper objectMapper;


    MeasurementsBundle serializeMeasurements(String measurementsBundleString) {
        try {
            return objectMapper.readValue(measurementsBundleString, MeasurementsBundle.class);
        } catch (IOException e) {
            log.error("Cannot serialize measurementsBundleString: '{}'", measurementsBundleString);
            throw new MeasurementsDataIntegrityViolationException("Cannot serialize measurementsBundleString '" + measurementsBundleString + "'", e);
        }
    }

    @SneakyThrows(IOException.class)
    String deserializeMeasurements(MeasurementsBundle measurementsBundle) {
        return objectMapper.writeValueAsString(measurementsBundle);
    }
}
