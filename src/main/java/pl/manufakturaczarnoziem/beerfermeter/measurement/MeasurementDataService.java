package pl.manufakturaczarnoziem.beerfermeter.measurement;

import static java.util.stream.Collectors.toCollection;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.manufakturaczarnoziem.beerfermeter.api.MeasurementApiData;
import pl.manufakturaczarnoziem.beerfermeter.api.MeasurementsBundleApiData;
import pl.manufakturaczarnoziem.beerfermeter.measurement.exception.MeasurementsDataIntegrityViolationException;
import pl.manufakturaczarnoziem.beerfermeter.measurement.exception.MeasurementsSavingException;
import pl.manufakturaczarnoziem.beerfermeter.process.ProcessService;
import pl.manufakturaczarnoziem.beerfermeter.sensor.Sensor;
import pl.manufakturaczarnoziem.beerfermeter.sensor.SensorRepository;

@Slf4j
@RequiredArgsConstructor
@Service
class MeasurementDataService {

    private final ProcessService processService;
    private final SensorRepository sensorRepository;
    private final MeasurementRepository measurementRepository;
    private final MeasurementsBundleSerializer measurementsBundleSerializer;


    void saveMeasurement(Long deviceId, MeasurementApiData measurementApiData) {
        MeasurementsBundleApiData singleMeasurementBundleApiData = createSingleMeasurementBundleApiData(measurementApiData);
        saveMeasurementsBundle(deviceId, singleMeasurementBundleApiData);
    }

    @Transactional
    void saveMeasurementsBundle(Long deviceId, MeasurementsBundleApiData measurementBundleApiData) {
        Long sensorId = measurementBundleApiData.getSensorId();
        Sensor sensor = sensorRepository.getOne(sensorId);

        List<Measurements> measurementsList = measurementRepository.findBySensor(sensor);

        if (measurementsList.size() <= 1) {
            if (!processService.isAnyProcessActiveForDevice(deviceId)) {
                throw new MeasurementsSavingException("There is no active process for deviceId: '" + deviceId + "'");
            }

            Measurements measurements = getOrCreateMeasurements(measurementsList, sensor);

            String measurementsBundleString = addMeasurementsToMeasurementsBundle(measurements, measurementBundleApiData.getMeasurements());
            measurements.setMeasurementsBundle(measurementsBundleString);
            measurementRepository.save(measurements);

        } else {
            throw new MeasurementsDataIntegrityViolationException("There is more than one measurement for sensorId: '" + sensorId + "'");
        }
    }

    private MeasurementsBundleApiData createSingleMeasurementBundleApiData(MeasurementApiData measurementApiData) {
        LinkedHashSet<Measurement> singleMeasurementSet
                = Stream.of(measurementApiData.getMeasurement()).collect(toCollection(LinkedHashSet::new));
        return new MeasurementsBundleApiData(measurementApiData.getSensorId(), singleMeasurementSet);
    }

    private Measurements getOrCreateMeasurements(List<Measurements> measurementsList, Sensor sensor) {
        return measurementsList.isEmpty() ? Measurements.builder().sensor(sensor).build() : measurementsList.get(0);
    }

    private String addMeasurementsToMeasurementsBundle(Measurements measurements, LinkedHashSet<Measurement> measurementsApiData) {
        MeasurementsBundle measurementsBundle = measurementsBundleSerializer.serializeMeasurements(measurements.getMeasurementsBundle());

        measurementsApiData
                .forEach(measurementApiValues -> {
                    measurementsBundle.getMeasurements().add(new Measurement(
                            measurementApiValues.getTime(),
                            measurementApiValues.getValue()));
                });

        return measurementsBundleSerializer.deserializeMeasurements(measurementsBundle);
    }
}
