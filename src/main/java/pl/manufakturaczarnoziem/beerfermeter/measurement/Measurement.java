package pl.manufakturaczarnoziem.beerfermeter.measurement;

import java.math.BigDecimal;

import lombok.Value;

@Value
public class Measurement {

    private Long time;
    private BigDecimal value;
}
