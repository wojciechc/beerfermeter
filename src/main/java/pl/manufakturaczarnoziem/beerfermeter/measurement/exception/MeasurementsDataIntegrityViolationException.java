package pl.manufakturaczarnoziem.beerfermeter.measurement.exception;

public class MeasurementsDataIntegrityViolationException extends MeasurementsSavingException {

    public MeasurementsDataIntegrityViolationException() {
        super();
    }

    public MeasurementsDataIntegrityViolationException(String message) {
        super(message);
    }

    public MeasurementsDataIntegrityViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
