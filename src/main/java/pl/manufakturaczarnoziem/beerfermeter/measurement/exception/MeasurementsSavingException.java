package pl.manufakturaczarnoziem.beerfermeter.measurement.exception;

public class MeasurementsSavingException extends RuntimeException {

    public MeasurementsSavingException() {
        super();
    }

    public MeasurementsSavingException(String message) {
        super(message);
    }

    public MeasurementsSavingException(String message, Throwable cause) {
        super(message, cause);
    }
}