package pl.manufakturaczarnoziem.beerfermeter.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.manufakturaczarnoziem.beerfermeter.measurement.Measurement;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MeasurementApiData {
    private Long sensorId;
    private Measurement measurement;
}
