package pl.manufakturaczarnoziem.beerfermeter.api;

import java.util.LinkedHashSet;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.manufakturaczarnoziem.beerfermeter.measurement.Measurement;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MeasurementsBundleApiData {
    private Long sensorId;
    private LinkedHashSet<Measurement> measurements;
}
