package pl.manufakturaczarnoziem.beerfermeter.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ProcessDetailsApiData {

    private Long processId;
    private String processState;
    private Long createdAt;
    private Long deviceId;

}
