INSERT INTO device_health_report (device_health_report_id, signal_strength, battery_voltage) VALUES (1, 80, 3.34);

INSERT INTO device (device_id, user_id, device_health_report_id) VALUES ('1629f32a-369c-48e2-bf87-63f0f9e9bb52', 1, 1);

INSERT INTO sensor_type (sensor_type_id, name, unit) VALUES (1, 'temperature', '*C');
INSERT INTO sensor_type (sensor_type_id, name, unit) VALUES (2, 'gravity', '*blg');

INSERT INTO sensor (sensor_id, sensor_type_id, device_id) VALUES ('84639758-2120-4fa4-8f03-133246844c76', 1, '1629f32a-369c-48e2-bf87-63f0f9e9bb52');
INSERT INTO sensor (sensor_id, sensor_type_id, device_id) VALUES ('fbd30258-184d-433d-9ae9-9861f19daed3', 2, '1629f32a-369c-48e2-bf87-63f0f9e9bb52');

INSERT INTO measurement (measurement_id, time, value, sensor_id) VALUES (1, '2019-03-25T11:50:00', 19.1, '84639758-2120-4fa4-8f03-133246844c76');
INSERT INTO measurement (measurement_id, time, value, sensor_id) VALUES (2, '2019-03-25T11:50:00', 12.5, 'fbd30258-184d-433d-9ae9-9861f19daed3');
INSERT INTO measurement (measurement_id, time, value, sensor_id) VALUES (3, '2019-03-25T12:50:00', 19.8, '84639758-2120-4fa4-8f03-133246844c76');
INSERT INTO measurement (measurement_id, time, value, sensor_id) VALUES (4, '2019-03-25T12:50:00', 10.2, 'fbd30258-184d-433d-9ae9-9861f19daed3');
