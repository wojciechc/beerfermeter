package pl.manufakturaczarnoziem.beerfermeter.process;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.google.common.collect.ImmutableList;
import pl.manufakturaczarnoziem.beerfermeter.device.Device;
import pl.manufakturaczarnoziem.beerfermeter.device.DeviceRepository;

class ProcessServiceTest {

    private static final Long DEVICE_ID_VALUE = 1L;
    private static final Device DEVICE = new Device();

    @Mock
    private ProcessRepository processRepository;
    @Mock
    private DeviceRepository deviceRepository;

    @InjectMocks
    private ProcessService underTest;


    void shouldReturnTrueIfSomeProcessIsActiveForDevice() {
        //given
        given(deviceRepository.getOne(DEVICE_ID_VALUE))
                .willReturn(DEVICE);
        given(processRepository.findByDevicesInAndState(ImmutableList.of(DEVICE), ProcessState.ACTIVE))
                .willReturn(ImmutableList.of(new Process()));

        //when
        boolean actualResult = underTest.isAnyProcessActiveForDevice(DEVICE_ID_VALUE);

        //then
        assertThat(actualResult).isTrue();
    }
}